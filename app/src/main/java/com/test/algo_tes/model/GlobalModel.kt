package com.test.algo_tes.model

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.HttpException
import java.lang.Exception


object GlobalModel {
        data class Error(
                override val name: String,
                override val message: String,
                override var data: Any?
        ) : IError
}

inline fun <reified T: IError> Throwable.generate(): T {
        val gson = Gson()
        if (this is HttpException) {
                val errorJsonString = this.response()?.errorBody()?.string()
                Log.d("error", errorJsonString.toString())
                var error: T
                try {
                        this.response()?.errorBody()?.close()
                        error = GsonBuilder().create().fromJson(errorJsonString, T::class.java)
                }
                catch (e: Exception){
                        val gerror = GsonBuilder().create().fromJson(errorJsonString, GlobalModel.Error::class.java)
                        gerror.data = null
                        error = GsonBuilder().create().fromJson(gson.toJson(gerror), T::class.java)
                }
                this.response()?.errorBody()?.close()
                return error
        }
        else{
                val message = " "
                val gError = GlobalModel.Error("", message, null)
                return GsonBuilder().create().fromJson(gson.toJson(gError), T::class.java)
        }
}