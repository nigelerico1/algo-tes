package com.test.algo_tes.model

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.Exception

interface IError {
    val name: String
    val message: String
    val data: Any?

    fun isEmpty() : Boolean{
        if (data === null) {
            return true
        }
        val map = data.serializeToMap()
        var empty = true
        try {
            map.values.forEach {
                if (it is ArrayList<*> && it.isNotEmpty()){
                    empty = false
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return empty
    }

    fun firstError() : String{
        if (data === null) {
            return ""
        }
        val map = data.serializeToMap()
        var message = ""
        try {
            map.values.forEach {
                if (it is ArrayList<*> && it.isNotEmpty()){
                    message = it.get(0) as String
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return message
    }
}

val gson = Gson()

fun <T> T.serializeToMap(): Map<String, Any> {
    return convert()
}

inline fun <reified T> Map<String, Any>.toDataClass(): T {
    return convert()
}

inline fun <I, reified O> I.convert(): O {
    val json = gson.toJson(this)
    return gson.fromJson(json, object : TypeToken<O>() {}.type)
}