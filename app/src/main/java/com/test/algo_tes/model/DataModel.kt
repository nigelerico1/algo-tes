package com.test.algo_tes.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


object DataModel {
    data class Result(val success: String, val data: Data)

    @Parcelize
    data class Data(
        val memes: List<Memes>?,
    ) : Parcelable

    @Parcelize
    data class Memes(
        val id: String,
        val name: String,
        val url: String,
        val width: Int,
        val height: Int,
        val box_count: Int,
    ) : Parcelable

    data class Error(
        override val name: String,
        override val message: String,
        override val data: ListError?
    ) : IError
    data class ListError(val status: List<String>)
}