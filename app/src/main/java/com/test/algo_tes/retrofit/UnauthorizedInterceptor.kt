package com.test.algo_tes.retrofit

import android.content.Context
import androidx.annotation.NonNull
import okhttp3.Interceptor
import okhttp3.Response
import org.greenrobot.eventbus.EventBus

internal class UnauthorizedInterceptor(val context: Context) : Interceptor {

    override fun intercept(@NonNull chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        if (response.code() === 401) {
            EventBus.getDefault().post(UnauthorizedEvent.instance())

        }
        return response
    }
}