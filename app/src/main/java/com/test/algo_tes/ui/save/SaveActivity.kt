package com.test.algo_tes.ui.save

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.algo_tes.R
import com.test.algo_tes.databinding.ActivityDetailBinding
import com.test.algo_tes.databinding.ActivityMainBinding
import com.test.algo_tes.databinding.ActivitySaveBinding
import com.test.algo_tes.ui.main.MainActivityViewModel
import com.test.algo_tes.ui.main.MainActivityViewModelFactory
import com.test.algo_tes.ui.share.ShareActivity
import java.io.File
import java.io.FileOutputStream

class SaveActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySaveBinding
    private lateinit var viewModel: SaveViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySaveBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar?.elevation = 0f
        supportActionBar?.title = "Save Meme"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProvider(this, SaveViewModelFactory(this))[SaveViewModel::class.java]

        val uri = intent.getParcelableExtra<Uri>("watermarkedBitmapUri")

        val inputStream = contentResolver.openInputStream(uri!!)
        val bitmap = BitmapFactory.decodeStream(inputStream)
        inputStream?.close()

        if (bitmap != null) {
            binding.imageView.setImageBitmap(bitmap)
        }

        observeSaveStatus()

        binding.saveButton.setOnClickListener {
            saveImage(bitmap!!, "WatermarkedImage")
        }

        binding.shareButton.setOnClickListener {
            val bitmap = (binding.imageView.drawable as BitmapDrawable).bitmap

            val file = File.createTempFile("tempImage", null, cacheDir)
            val fileOutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            fileOutputStream.close()

            val uri = FileProvider.getUriForFile(this, "${packageName}.provider", file)

            val intent = Intent(this, ShareActivity::class.java)
            intent.putExtra("watermarkedBitmapUri", uri)
            startActivity(intent)

        }


    }

    private fun saveImage(bitmap: Bitmap, title: String) {
        viewModel.saveImageToGallery(bitmap, title)
    }

    private fun observeSaveStatus() {
        viewModel.saveStatus.observe(this, Observer { saved ->
            if (saved) {
                Toast.makeText(this, "Image saved successfully", Toast.LENGTH_SHORT).show()
                viewModel.resetSaveStatus()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}