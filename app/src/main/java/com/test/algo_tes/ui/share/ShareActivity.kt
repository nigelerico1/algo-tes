package com.test.algo_tes.ui.share

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import com.test.algo_tes.databinding.ActivityShareBinding
import java.io.ByteArrayOutputStream

class ShareActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShareBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShareBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar?.elevation = 0f
        supportActionBar?.title = "Share Meme"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val uri = intent.getParcelableExtra<Uri>("watermarkedBitmapUri")

        val inputStream = contentResolver.openInputStream(uri!!)
        val bitmap = BitmapFactory.decodeStream(inputStream)
        inputStream?.close()

        if (bitmap != null) {
            binding.imageView.setImageBitmap(bitmap)
        }

        binding.shareTwitterButton.setOnClickListener {
            shareToTwitter(bitmap)
        }

        binding.shareFaceBookButton.setOnClickListener {
            shareToFacebook(bitmap)
        }

    }

    private fun shareToTwitter(bitmap: Bitmap) {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Ini gambarnya")
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(contentResolver, bitmap, "Title", null)
        val imageUri = Uri.parse(path)
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
        shareIntent.setPackage("com.twitter.android")
        startActivity(Intent.createChooser(shareIntent, "Share to Twitter"))
    }

    private fun shareToFacebook(bitmap: Bitmap) {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "image/*"
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(contentResolver, bitmap, "Title", null)
        val imageUri = Uri.parse(path)
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Ini gambarnya")
        shareIntent.setPackage("com.facebook.katana") // Package name for Facebook
        startActivity(Intent.createChooser(shareIntent, "Share to Facebook"))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}