package com.test.algo_tes.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.algo_tes.databinding.ActivityMainBinding
import com.test.algo_tes.ui.detail.DetailActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel

    private val mainAdapter by lazy(LazyThreadSafetyMode.NONE) {
        MainAdapter(onProductClick())
    }

    private fun onProductClick() = object : MainAdapter.OnItemClickListener {
        override fun onOngoingClick(url: String) {
            val intent = Intent(this@MainActivity, DetailActivity::class.java)
            intent.putExtra("url", url)
            startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar?.elevation = 0f
        supportActionBar?.title = "Meme Gallery"

        viewModel = ViewModelProvider(this, MainActivityViewModelFactory(this))[MainActivityViewModel::class.java]

        setupView()
        setupObserver()
        setupProductList()

    }


    private fun setupObserver() = with(viewModel) {
        item.observe(this@MainActivity) {
            it.data.memes?.let(mainAdapter::addAllProductData)
            if (it.data.memes == null) {
                mainAdapter.clear()
                Toast.makeText(
                    this@MainActivity,
                    "Data tidak ditemukan.",
                    Toast.LENGTH_SHORT
                ).show()
            }
            if (mainAdapter.itemCount == 0) {
                binding.rv.visibility = View.GONE
                binding.textNotFound.visibility = View.VISIBLE
            } else {
                binding.rv.visibility = View.VISIBLE
                binding.textNotFound.visibility = View.GONE
            }
        }

        loading.observe(this@MainActivity) {
            if (it) {
                binding.rv.visibility = View.GONE
                binding.progressBar.visibility = View.VISIBLE
                binding.textNotFound.visibility = View.GONE
            } else {
                binding.progressBar.visibility = View.GONE
                binding.rv.visibility = View.VISIBLE
            }
        }

        loadingRefresh.observe(this@MainActivity) {
            binding.swipeRefreshLayout.isRefreshing = it
        }


        error.observe(this@MainActivity) {
            mainAdapter.clear()
            Toast.makeText(
                this@MainActivity,
                "Terjadi kesalahan jaringan!",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    private fun setupView() {
        viewModel.loadItem()
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.loadItem()
        }
    }

    private fun setupProductList() = with(binding.rv) {
        layoutManager = GridLayoutManager(this@MainActivity, 3)
        setupAdapter(mainAdapter)
    }


    private fun RecyclerView.setupAdapter(
        viewAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    ) {
        setHasFixedSize(true)
        adapter = viewAdapter
    }




    override fun onDestroy() {
        viewModel.dispose()
        super.onDestroy()
    }



}