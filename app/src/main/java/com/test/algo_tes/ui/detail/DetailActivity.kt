package com.test.algo_tes.ui.detail

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import com.test.algo_tes.databinding.ActivityDetailBinding
import com.test.algo_tes.ui.save.SaveActivity
import java.io.File
import java.io.FileOutputStream


class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: DetailViewModel
    private var selectedImageUri: android.net.Uri? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar?.elevation = 0f
        supportActionBar?.title = "Meme Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProvider(this, DetailViewModelFactory(this))[DetailViewModel::class.java]

        binding.applyWatermarkButton.setOnClickListener {
            applyWatermark()
        }

        binding.selectImageFromGalleryButton.setOnClickListener {
            openGallery()
        }

        binding.nextButton.setOnClickListener {
            val bitmap = (binding.imageView.drawable as BitmapDrawable).bitmap

            val file = File.createTempFile("tempImage", null, cacheDir)
            val fileOutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            fileOutputStream.close()

            val uri = FileProvider.getUriForFile(this, "${packageName}.provider", file)

            val intent = Intent(this, SaveActivity::class.java)
            intent.putExtra("watermarkedBitmapUri", uri)
            startActivity(intent)

        }

        val url = intent.getStringExtra("url")
        loadInitialImage(url!!)

    }

    private fun loadInitialImage(imageUrl: String) {
        viewModel.loadImageFromUrl(applicationContext, imageUrl)
        observeWatermarkedBitmap()
    }

    private fun applyWatermark() {
        viewModel.addWatermarkToImage(selectedImageUri, binding.watermarkTextEditText.text.toString())


    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        galleryLauncher.launch(intent)
    }

    private fun observeWatermarkedBitmap() {
        viewModel.watermarkedBitmap.observe(this) { watermarkedBitmap ->
            binding.imageView.setImageBitmap(watermarkedBitmap)
        }
    }



    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            result.data?.data?.let { uri ->
                selectedImageUri = uri
                applyWatermark()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}