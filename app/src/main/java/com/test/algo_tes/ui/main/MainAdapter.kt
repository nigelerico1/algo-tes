package com.test.algo_tes.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.test.algo_tes.databinding.ItemListBinding
import com.test.algo_tes.model.DataModel

class MainAdapter(val onClickListener: OnItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    lateinit var context: Context
    private val dataList = mutableListOf<DataModel.Memes>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        DataViewHolder.from(parent)


    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as DataViewHolder).bind(dataList[position], onClickListener)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        dataList.clear()
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAllProductData(data: List<DataModel.Memes>) {
        dataList.clear()
        data.forEach { item ->
            dataList.add(item)
        }
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onOngoingClick(url: String)
    }

    private class DataViewHolder private constructor(
        private val binding: ItemListBinding,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: DataModel.Memes, onItemClick: OnItemClickListener) {
            bindView(item)

            binding.imageView.setOnClickListener {
                onItemClick.onOngoingClick(
                    url = item.url
                )
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        private fun bindView(item: DataModel.Memes) = with(binding) {
            Glide.with(context)
                .load(item.url)
                .centerCrop()
                .into(imageView)


        }

        companion object {
            @JvmStatic
            fun from(parent: ViewGroup) = DataViewHolder(
                ItemListBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false),
                parent.context
            )

        }
    }
}