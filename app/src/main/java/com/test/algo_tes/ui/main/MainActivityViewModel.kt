package com.test.algo_tes.ui.main

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.algo_tes.model.DataModel
import com.test.algo_tes.model.generate
import com.test.algo_tes.retrofit.INodeJS
import com.test.algo_tes.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

class MainActivityViewModel(context: Context) : ViewModel() {
    private val retrofit: Retrofit = RetrofitClient.create(context)
    private var myAPI: INodeJS = retrofit.create(INodeJS::class.java)

    private val _item = MutableLiveData<DataModel.Result>()
    val item: LiveData<DataModel.Result> = _item

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _loadingRefresh = MutableLiveData<Boolean>()
    val loadingRefresh: LiveData<Boolean> = _loadingRefresh

    private val _error = MutableLiveData<DataModel.Error>()
    val error: LiveData<DataModel.Error> = _error

    private var compositeDisposable = CompositeDisposable()


    fun loadItem() {
        _loading.value = true
        compositeDisposable.add(
            myAPI.getItem().subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe({ t ->
                _loading.value = false
                _loadingRefresh.value = false
                _item.value = t
            }, { throwable ->
                _loading.value = false
                _loadingRefresh.value = false
                _error.value = throwable.generate()
                throwable.printStackTrace()
            })
        )
    }

    fun dispose(){
        compositeDisposable.dispose()
    }

}