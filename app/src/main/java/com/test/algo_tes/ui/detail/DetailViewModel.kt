package com.test.algo_tes.ui.detail

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DetailViewModel(val context: Context) : ViewModel() {
    private val _watermarkedBitmap = MutableLiveData<Bitmap>()
    val watermarkedBitmap: LiveData<Bitmap> = _watermarkedBitmap

    fun loadImageFromUrl(context: Context, imageUrl: String) {
        viewModelScope.launch {
            val bitmap = downloadBitmapFromUrl(context, imageUrl)
            _watermarkedBitmap.value = bitmap
        }
    }

    fun addWatermarkToImage(selectedImageUri: Uri?, watermarkText: String) {
        val currentBitmap = _watermarkedBitmap.value
        if (currentBitmap != null && selectedImageUri != null) {
            viewModelScope.launch {
                val bitmapFromGallery = loadImageFromGallery(selectedImageUri)
                val watermarkedBitmap = addWatermark(currentBitmap, bitmapFromGallery, watermarkText)
                _watermarkedBitmap.value = watermarkedBitmap
            }
        } else {
            addWatermarkTextOnly(watermarkText)
        }
    }

    private fun addWatermarkTextOnly(watermarkText: String) {
        val currentBitmap = _watermarkedBitmap.value
        if (currentBitmap != null) {
            viewModelScope.launch {
                val watermarkedBitmap = addTextWatermark(currentBitmap, watermarkText)
                _watermarkedBitmap.value = watermarkedBitmap
            }
        }
    }

    private suspend fun downloadBitmapFromUrl(context: Context, imageUrl: String): Bitmap {
        return withContext(Dispatchers.IO) {
            Glide.with(context)
                .asBitmap()
                .load(imageUrl)
                .submit()
                .get()
        }
    }

    private suspend fun loadImageFromGallery(uri: Uri): Bitmap {
        return withContext(Dispatchers.IO) {
            Glide.with(context)
                .asBitmap()
                .load(uri)
                .submit()
                .get()
        }
    }

    private fun addWatermark(originalBitmap: Bitmap, watermarkBitmap: Bitmap, watermarkText: String): Bitmap {
        val result = originalBitmap.copy(originalBitmap.config, true)
        val canvas = Canvas(result)
        val paint = Paint()
        paint.color = Color.WHITE
        paint.textSize = 48f

        val scaledWidth = originalBitmap.width / 4
        val scaledHeight = (scaledWidth.toFloat() / watermarkBitmap.width.toFloat() * watermarkBitmap.height).toInt()
        val scaledWatermark = Bitmap.createScaledBitmap(watermarkBitmap, scaledWidth, scaledHeight, true)

        val xPos = (result.width - scaledWatermark.width) / 2
        val yPos = 20f

        canvas.drawBitmap(scaledWatermark, xPos.toFloat(), yPos, null)

        val textXPos = 20f
        val textYPos = (canvas.height - 20).toFloat()
        canvas.drawText(watermarkText, textXPos, textYPos, paint)

        return result
    }

    private fun addTextWatermark(originalBitmap: Bitmap, watermarkText: String): Bitmap {
        val result = originalBitmap.copy(originalBitmap.config, true)
        val canvas = Canvas(result)
        val paint = Paint()
        paint.color = Color.WHITE
        paint.textSize = 48f

        val textXPos = 20f
        val textYPos = (canvas.height - 20).toFloat()
        canvas.drawText(watermarkText, textXPos, textYPos, paint)

        return result
    }


}