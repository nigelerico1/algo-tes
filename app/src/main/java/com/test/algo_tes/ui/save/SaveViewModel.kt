package com.test.algo_tes.ui.save

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream


class SaveViewModel(val context: Context) : ViewModel() {
    private val _saveStatus = MutableLiveData<Boolean>()
    val saveStatus: LiveData<Boolean> get() = _saveStatus

    fun saveImageToGallery(bitmap: Bitmap, title: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val image = File(imagesDir, "$title.jpg")
            val fos = FileOutputStream(image)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.close()

            _saveStatus.postValue(true)
        }
    }

    fun resetSaveStatus() {
        _saveStatus.value = false
    }
}