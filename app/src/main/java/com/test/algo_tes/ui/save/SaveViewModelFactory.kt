package com.test.algo_tes.ui.save

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.algo_tes.ui.main.MainActivityViewModel

class SaveViewModelFactory  (private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SaveViewModel::class.java)) {
            return SaveViewModel(context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}